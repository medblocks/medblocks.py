from medblocks import Client
from medblocks import cryptography
from io import BytesIO
import requests
import json
import time
import unittest
from ipfshttpclient.exceptions import ConnectionError 

IPFS_NODE = "ipfs"
NODE = "go-medblocks"

class ClientTests(unittest.TestCase):
    def setUp(self):
        self.api = Client(NODE, IPFS_NODE)
        self.api.nuke()
        self.api.register("testemail@gmail.com","testpassword")
        self.api.register("anotherguy@sdfsd.com", "randompassword")
        self.api.login("testemail@gmail.com","testpassword")
        response = self.api.addBlock("LOLOLOLOL".encode(), "testemail@gmail.com", "testBlock")
        self.hash = json.loads(response[0]["message"])["ipfsHash"]
        self.api.logout()

    def tearDown(self):
        self.api.nuke()

    def test_keygen(self):
        _, private = cryptography.generate_RSA()
        encryptedKey, iv = cryptography.encryptKey(private, "Test pass")
        newkey = cryptography.decryptKey(encryptedKey, iv, "Test pass")
        self.assertEqual(newkey, private)
    
    def test_register(self):
        register = self.api.register("testemail2@gmail.com","testpassword2")
        print(register)
        user = self.api.medblocks("user/{}".format("testemail2@gmail.com"),"GET")
        print(user)
        keys = self.api.medblocks("key/{}".format("testemail2@gmail.com"),"GET")
        a = self.api.getPublicKey("testemail2@gmail.com")
        print(a)
        print(keys)
        self.assertEqual(user["ePublicKey"], register["ePublicKey"])
        self.api.login("testemail2@gmail.com","testpassword2")
        self.assertEqual(self.api.sPublicKey, register["sPublicKey"].encode())
    
    def test_list(self):
        return
        # TODO
        self.assertTrue(True)

    def test_login(self):
        self.api.login("testemail@gmail.com", "testpassword")
        self.assertTrue(self.api._check_login())

    def test_getBlock(self):
        self.api.login("testemail@gmail.com", "testpassword")
        self.api.getBlock(self.hash)

    def test_give_permission(self):
        to = "anotherguy@sdfsd.com"
        hash = self.hash
        self.api.login("testemail@gmail.com", "testpassword")
        self.api.addPermission(hash, to)
        self.api.logout()
        self.api.login("anotherguy@sdfsd.com", "randompassword")
        self.api.getBlock(hash)

    def test_fhir(self):
        start = time.time()
        self.api.login("testemail@gmail.com", "testpassword")
        with open("synthetic_fhir.json", "r") as f:
            data = json.load(f)
        self.api.addFhir(data, max_workers=100)
        print("Total elapsed for 512 FHIR records {}".format(round(time.time()-start, 2)))

if __name__ == '__main__':
    time.sleep(15)
    while True:
        try:
            c = Client(NODE, IPFS_NODE)
            print("Got host!")
            break
        except ConnectionError:
            time.sleep(3)
            print("Waiting 3 secs for container...")
    unittest.main()

