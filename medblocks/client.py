import ipfshttpclient
import json
from json.decoder import JSONDecodeError
from medblocks import cryptography
from medblocks.errors import *
import requests
from concurrent.futures import ThreadPoolExecutor, as_completed
import time, tempfile, os

IPFS_PORT = "5001"
MEDBLOCKS_PORT = "8080"

    
class Client(object):
    def __init__(self, ip_address, ipfs_address=None, *args, **kwargs):
        if ipfs_address is None:
            ipfs_address = ip_address
        self.reference_mapping = {}
        self.base_url = "http://{ip_address}:{port}/".format(ip_address=ip_address, port=MEDBLOCKS_PORT)
        
        self.ipfs = ipfshttpclient.connect("/dns/{ipfs_address}/tcp/{port}/http".format(ipfs_address=ipfs_address, port=IPFS_PORT))
        self.logged_in = False
        
    def _check_login(self):
        if self.logged_in:
            return True
        else:
            raise LoginError("Please login and then try again")
    
    def nuke(self):
        """Nukes the whole database. Only for testing purposes"""
        if requests.post("{}nuke".format(self.base_url)).text == "Ok":
            return True
        else:
            raise Exception("Nuke not okay")

    def medblocks(self, endpoint, method="POST", json=None):
        url = self.base_url + "{}".format(endpoint)
        switcher = {
            "POST": requests.post,
            "GET": requests.get,
            "PUT": requests.put
        }
        requestFunc = switcher.get(method, requests.post)
        r = requestFunc(url, json=json)
        try:
            r.raise_for_status()
            return r.json()
        except requests.exceptions.HTTPError: # TODO Check backend for JSON in errors
            raise MedBlocksAPIError(r.text)

    def _checkReferences(dict):
        """Checks dictionary for references and replaces them with known references or throws error"""

    def list(self, **kwargs):
        """
        Optional kwargs:
        ownerEmailId (string): Lists all medblocks that belong to the owner
        permittedEmailId (string): List all medblocks that this email has permission to
        """
        return self.medblocks("blocks", "GET", kwargs)
    
    def register(self, emailId, password):
        """
        Generates a key pair, encrypts with as password and stores it on the server. Registers a 
        """
        keypairs = cryptography.generateKeys(password)
        keypairs["emailId"] = emailId
        self.medblocks("key", "POST", keypairs)
        self.login(keypairs["emailId"], password, keypairs)
        payload = self.prepare_payload({
            "emailId": emailId,
            "sPublicKey": keypairs["sPublicKey"],
            "ePublicKey": keypairs["ePublicKey"]
            })
        return self.medblocks("user", "POST", payload)
    
    def login(self, emailId, password, offlineKeys=None):
        if offlineKeys is None:
            keys = self.medblocks("key/{}".format(emailId), "GET")
        else:
            keys = offlineKeys
        self.ePrivateKey = cryptography.decryptKey(keys["ePrivateKey"].encode(), keys["IV"]["IVE"].encode(), password)
        self.sPrivateKey = cryptography.decryptKey(keys["sPrivateKey"].encode(), keys["IV"]["IVS"].encode(), password)
        self.ePublicKey = cryptography.get_publicKey(self.ePrivateKey)
        self.sPublicKey = cryptography.get_publicKey(self.sPrivateKey)
        self.emailId = emailId
        self.logged_in = True
        return
    
    def getPublicKey(self, email):
        # if email == self.emailId:
        #     return self.ePublicKey
        user = self.medblocks("user/{}".format(email), "GET")
        return user["ePublicKey"]

    def prepare_payload(self, dict):
        data = json.dumps(dict)
        signature = cryptography.generate_signature(data, self.sPrivateKey)
        payload = {"data":data, "signature": signature.decode()}
        return payload

    def addBlock(self, bytes, to_address, name):
        self._check_login()     
        public_key = self.getPublicKey(to_address)
        aes_key = cryptography.generate_aes_key()
        encrypted_file, iv = cryptography.encrypt_file(bytes, aes_key)
        access_key = cryptography.create_access_key(aes_key, public_key)   
        r = self.ipfs.add_bytes(encrypted_file)
        iv = iv.decode()
        data = [{
            "ipfsHash": r,
            "IV": iv,
            "name": name,
            "format": "fhir/json",
            "ownerEmailId": self.emailId,
            "signatoryEmailId": self.emailId,
            "permissions":[
            {"receiverEmailId": to_address, 
            "receiverKey":access_key.decode()}
            ]
        }]
        payload = self.prepare_payload(data)
        medblock = self.medblocks("blocks", "POST", payload)
        return medblock
    
    def extractKey(self, block):
        self._check_login()
        permissions = block["permissions"]
        for p in permissions:
            if p["receiverEmailId"] == self.emailId:
                accessKey = p["receiverKey"]
        try:
            aes_key = cryptography.decrypt_access_key(accessKey, self.ePrivateKey)
            return aes_key
        except NameError:
            raise PermissionError("No access key for current user found. To view raw block set raw=True")

    def getBlock(self, ipfsHash, raw=False):
        block = self.medblocks("block/{}".format(ipfsHash), "GET")
        if raw:
            return block
        aes_key = self.extractKey(block)
        iv = block["IV"].encode()
        encrypted = self.ipfs.cat(ipfsHash)
        decrypted = cryptography.decrypt_file(encrypted, aes_key, iv)
        return decrypted

    def addPermission(self, ipfsHash, to):
        self._check_login()
        public_key = self.getPublicKey(to)
        block = self.medblocks("block/{}".format(ipfsHash), "GET")
        aes_key = self.extractKey(block)
        accessKey = cryptography.create_access_key(aes_key, public_key)
        data = {
            "ipfsHash": ipfsHash,
            "senderEmailId": self.emailId,
            "permissions": [
                {
                    "receiverEmailId": to,
                    "receiverKey": accessKey.decode()
                }
            ]
        }
        payload = self.prepare_payload(data)
        return self.medblocks("block/permissions", "PUT", payload)

    def logout(self):
        self.ePublicKey = None
        self.sPublicKey = None
        self.ePrivateKey = None
        self.sPrivateKey = None
        self.logged_in = False
        return

    def bulk_add_ipfs(self):
        start = time.time()
        with tempfile.TemporaryDirectory() as dirpath:
            # TODO: Add threading for faster encryption and write
            for uuid in self.reference_mapping.keys():
                data = self.reference_mapping[uuid]
                bytes = json.dumps(data).encode()
                aes_key = cryptography.generate_aes_key()
                encrypted_file, iv = cryptography.encrypt_file(bytes, aes_key)
                access_key = cryptography.create_access_key(aes_key, self.ePublicKey)
                with open(os.path.join(dirpath, uuid), "wb") as f:
                    f.write(encrypted_file)
                data = {
                    "ipfsHash": None,
                    "IV": iv.decode(),
                    "name": uuid,
                    "format": "fhir/json",
                    "signatoryEmailId": self.emailId,
                    "ownerEmailId": self.emailId,
                    "permissions":[
                        {
                            "receiverEmailId": self.emailId, 
                            "receiverKey":access_key.decode()
                            }
                        ]
                    }
                self.reference_mapping[uuid] = data
            hash_list = self.ipfs.add(dirpath)
            hash_list = {item["Name"].split("/")[1] : item["Hash"] for item in hash_list[:-1]}
            for uuid in self.reference_mapping.keys():
                self.reference_mapping[uuid]["ipfsHash"] = hash_list[uuid]
            end = time.time()

        assert self.check_reference_list()
        # print("encryption and ipfs add completed for {} blocks in {} secs".format(len(hash_list), round(end - start, 2)))
        return self.reference_mapping
        
    def check_reference_list(self):
        for data in self.reference_mapping.values():
            try:
                if data["ipfsHash"] is None:
                    raise ReferenceListIncomplete("All items do not have ipfs hash")
            except KeyError:
                raise ReferenceListIncomplete("ipfs process not run")
        return True
    
    def async_payload_add(self, data):
        payload = self.prepare_payload(data)
        medblock = self.medblocks("blocks", "POST", payload)
        return medblock
    
    def bulk_add_medblocks(self):
        # Not implemented on go-medblocks
        start_time = time.time()
        self.check_reference_list()
        payload = self.prepare_payload(list(self.reference_mapping.values()))
        response = self.medblocks("blocks", "POST", payload)
        elapsed = round(time.time() - start_time, 2)
        print("addBlock completed for {} in {} secs".format(len(self.reference_mapping), elapsed))
        return response

    def async_add_medblock(self, max_workers):
        self.check_reference_list()
        start_time = time.time()
        futures = {}
        with ThreadPoolExecutor(max_workers) as f:
            for uuid in self.reference_mapping.keys():
                data = self.reference_mapping[uuid]
                futures[uuid] = f.submit(self.async_payload_add, data)
        for uuid in futures.keys():
            self.reference_mapping[uuid] = futures[uuid].result()
        elapsed = round(time.time() - start_time, 2)
        print("addBlock completed for {} in {} secs".format(len(self.reference_mapping), elapsed))
    
    def addFhir(self, data, version="R4", max_workers=30):
        start = time.time()
        for resource in data["entry"]:
            resource.pop("fullUrl")
            uuid = resource["resource"].pop("id")
            self.reference_mapping[uuid] = resource
    
        self.bulk_add_ipfs()
        # self.async_add_medblock(max_workers=max_workers)
        self.bulk_add_medblocks()
        print("Finished {} blocks in {} secs".format(len(self.reference_mapping), round(time.time()-start, 2)))
        mapping = self.reference_mapping
        self.reference_mapping = {}
        return mapping