FROM python:latest
WORKDIR /src
ADD ./requirements.txt /src/requirements.txt
RUN pip install -r requirements.txt
ADD ./ /src/
CMD [ "python", "tests.py" ]